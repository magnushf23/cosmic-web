use crate::barneshut::BarnesHutTree;
use crate::gpu::Vertex;
use rand::prelude::*;
use rayon::iter::{IndexedParallelIterator, IntoParallelIterator, ParallelIterator};
use std::sync::Arc;
use std::{f64::consts::PI, time::Instant};

#[derive(Debug, Default, Clone)]
pub struct Cloud {
    pub size: usize,
    pub const_g: f64,
    pub point_mass: Vec<f64>,
    pub pos_x: Vec<f64>,
    pub pos_y: Vec<f64>,
    pub pos_z: Vec<f64>,
    pub vel_x: Vec<f64>,
    pub vel_y: Vec<f64>,
    pub vel_z: Vec<f64>,
}

pub fn screenshot(name: &str, values: Vec<Vec<u8>>) {
    match std::fs::remove_file(format!("{name}.png")) {
        Ok(()) => (),
        Err(e) => println!("Could not remove file because of : {:?}", e),
    };

    // Create a new ImgBuf with width: imgx and height: imgy
    let mut imgbuf = image::ImageBuffer::new(
        values.len().try_into().unwrap(),
        values[0].len().try_into().unwrap(),
    );
    // Iterate over the coordinates and pixels of the image
    for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
        let val = values[x as usize][y as usize];
        *pixel = image::Rgb([val, val, val]);
    }
    // Save the image as “fractal.png”, the format is deduced from the path
    imgbuf
        .save(format!("{name}.png"))
        .expect("Problem on saving image");
}

pub fn find_max_min<T: std::cmp::PartialOrd + Copy>(slice: &[T]) -> [T; 2] {
    let mut max = &slice[0];
    let mut min = &slice[0];

    for elem in slice.iter().skip(1) {
        if *elem < *min {
            min = elem;
        }
        if *elem > *max {
            max = elem;
        }
    }
    [*max, *min]
}

impl Cloud {
    pub fn new() -> Self {
        Cloud {
            size: 0,
            const_g: 1.,
            point_mass: Vec::<f64>::new(),
            pos_x: Vec::<f64>::new(),
            pos_y: Vec::<f64>::new(),
            pos_z: Vec::<f64>::new(),
            vel_x: Vec::<f64>::new(),
            vel_y: Vec::<f64>::new(),
            vel_z: Vec::<f64>::new(),
        }
    }
    pub fn fill_cube(
        &mut self,
        center_position: [f64; 3],
        cube_width: f64,
        particle_number: usize,
        mass: [f64; 2],
        velocity: [f64; 4],
    ) {
        self.size += particle_number;

        let mut rng = rand::thread_rng();

        for _n in 0..particle_number {
            self.pos_x
                .push((rng.gen::<f64>() - 0.5) * cube_width + center_position[0]);
            self.pos_y
                .push((rng.gen::<f64>() - 0.5) * cube_width + center_position[1]);
            self.pos_z
                .push((rng.gen::<f64>() - 0.5) * cube_width + center_position[2]);
            self.vel_x
                .push((rng.gen::<f64>() - 0.5) * velocity[3] * f64::sqrt(2.) * 2. + velocity[0]);
            self.vel_y
                .push((rng.gen::<f64>() - 0.5) * velocity[3] * f64::sqrt(2.) * 2. + velocity[1]);
            self.vel_z
                .push((rng.gen::<f64>() - 0.5) * velocity[3] * f64::sqrt(2.) * 2. + velocity[2]);
            self.point_mass
                .push((rng.gen::<f64>() - 0.5) * mass[1] * 2. + mass[0]);
        }
    }
    pub fn fill_ball(
        &mut self,
        center_position: [f64; 3],
        radius: f64,
        particle_number: usize,
        mass: [f64; 2],
        velocity: [f64; 4],
        moment: [f64; 3],
    ) {
        self.size += particle_number;

        let mut rng = rand::thread_rng();

        for _n in 0..particle_number {
            let r = rng.gen::<f64>().sqrt() * radius;
            let theta = rng.gen::<f64>() * PI;
            let phi = rng.gen::<f64>() * PI * 2.;
            self.pos_x
                .push(r * f64::sin(theta) * f64::cos(phi) + center_position[0]);
            self.pos_y
                .push(r * f64::sin(theta) * f64::sin(phi) + center_position[1]);
            self.pos_z.push(r * f64::cos(theta) + center_position[2]);
            self.vel_x.push(
                (rng.gen::<f64>() - 0.5) * velocity[3] * f64::sqrt(2.) * 2.
                    - f64::sin(theta) * f64::sin(phi) * moment[2]
                    + velocity[0],
            );
            self.vel_y.push(
                (rng.gen::<f64>() - 0.5) * velocity[3] * f64::sqrt(2.) * 2.
                    + f64::sin(theta) * f64::cos(phi) * moment[2]
                    + velocity[1],
            );
            self.vel_z
                .push((rng.gen::<f64>() - 0.5) * velocity[3] * f64::sqrt(2.) * 2. + velocity[2]);
            self.point_mass.push(
                ((rng.gen::<f64>() - 0.5) * mass[1] * 2. + mass[0]) / (particle_number as f64),
            );
        }
    }
    pub fn fill_disk(
        &mut self,
        center_position: [f64; 3],
        radius: f64,
        particle_number: usize,
        mass: [f64; 2],
        velocity: [f64; 4],
    ) {
        self.size += particle_number;

        let mut rng = rand::thread_rng();
        let mass_i = mass[0] / (particle_number as f64);

        for _n in 0..particle_number {
            let r = rng.gen::<f64>().sqrt() * radius;
            let theta = rng.gen::<f64>() * PI * 2.;
            self.pos_x.push(r * f64::cos(theta) + center_position[0]);
            self.pos_y.push(r * f64::sin(theta) + center_position[1]);
            self.pos_z.push(center_position[2]);
            let rot_vel = -(self.const_g * mass[0] as f64 / radius.powi(2) * r).sqrt() / 20.;
            self.vel_x.push(
                (rng.gen::<f64>() - 0.5) * velocity[3] * f64::sqrt(2.) * 2.
                    - f64::sin(theta) * rot_vel
                    + velocity[0],
            );
            self.vel_y.push(
                (rng.gen::<f64>() - 0.5) * velocity[3] * f64::sqrt(2.) * 2.
                    + f64::cos(theta) * rot_vel
                    + velocity[1],
            );
            self.vel_z
                .push((rng.gen::<f64>() - 0.5) * velocity[3] * f64::sqrt(2.) * 2. + velocity[2]);
            self.point_mass
                .push(((rng.gen::<f64>() - 0.5) * mass[1] * 2. + 1.) * mass_i);
        }
    }
    pub fn set_gravity_constant(&mut self, const_g: f64) {
        self.const_g = const_g;
    }
    fn acceleration(
        &self,
        x: &[f64],
        y: &[f64],
        z: &[f64],
        min_dist: f64,
        const_g: f64,
        theta_ratio: f64,
    ) -> [Vec<f64>; 3] {
        let mut ax = Vec::<f64>::with_capacity(self.size); // vec![0.; self.size];
        let mut ay = Vec::<f64>::with_capacity(self.size); //vec![0.; self.size];
        let mut az = Vec::<f64>::with_capacity(self.size); // vec![0.; self.size];

        // If theta is 0, Barnes-Hut algorithm is equivalent to the exact method (brute force), computing the attraction
        // between every pair of particles, in O(n^2) steps
        if theta_ratio == 0. {
            ax = vec![0.; self.size];
            ay = vec![0.; self.size];
            az = vec![0.; self.size];
            for p1 in 1..self.size {
                for p2 in 0..p1 {
                    // Distance is computed between two points, with an added minimum distance for simulation stability
                    let distance = ((x[p1] - x[p2]).powi(2)
                        + (y[p1] - y[p2]).powi(2)
                        + (z[p1] - z[p2]).powi(2))
                    .sqrt()
                        + min_dist;
                    let a = const_g * self.point_mass[p1] * self.point_mass[p2] / distance.powi(3);
                    ax[p1] = a * (x[p2] - x[p1]);
                    ay[p1] = a * (y[p2] - y[p1]);
                    az[p1] = a * (z[p2] - z[p1]);
                    ax[p2] = -ax[p1];
                    ay[p2] = -ay[p1];
                    az[p2] = -az[p1];
                }
            }
        } else {
            // Max and min positions of particles, to determine domain size
            let maxminx = find_max_min(x);
            let maxminy = find_max_min(y);
            let maxminz = find_max_min(z);
            // Random number, small enough to avoid having too many subdivisions when all particles are close together
            let offset = 1e-10;
            // Domain size large enough to hold all particles
            let domain_size = (maxminx[0] - maxminx[1])
                .max(maxminy[0] - maxminy[1])
                .max(maxminz[0] - maxminz[1])
                + 2. * offset;
            let mut tree = BarnesHutTree::with_size(
                [
                    maxminx[1] - offset,
                    maxminx[1] - offset,
                    maxminx[1] - offset,
                ],
                domain_size,
            );

            let _t1 = Instant::now();
            // Insert particles in tree
            // NOT PARALLEL
            tree.construct(x, y, z, &self.point_mass);
            //println!("Tree construction : {:} ms", t1.elapsed().as_millis());
            let _t2 = Instant::now();

            //let mut maxtime = 0;
            let arc_tree = Arc::<BarnesHutTree>::from(tree);

            let result: Vec<[f64; 3]> = x
                .into_par_iter()
                .zip(y)
                .zip(z)
                .zip(&self.point_mass)
                .map(|(((x, y), z), m)| {
                    arc_tree.compute_body_force(
                        &[*x, *y, *z],
                        &(const_g * m),
                        &min_dist,
                        &theta_ratio,
                    )
                })
                .collect();

            for force in result {
                ax.push(force[0]);
                ay.push(force[1]);
                az.push(force[2]);
            }

            //println!("Force calc : {:} ms", t2.elapsed().as_millis());
            //println!("Max time for individual force calc : {:} ms", maxtime);
        }
        [ax, ay, az]
    }
    fn next_step(&mut self, dt: f64, integration_substeps: usize, min_dist: f64, theta_ratio: f64) {
        if integration_substeps == 1 {
            let acc = self.acceleration(
                &self.pos_x,
                &self.pos_y,
                &self.pos_z,
                min_dist,
                1.,
                theta_ratio,
            );
            for p in 0..self.size {
                self.vel_x[p] += acc[0][p] * dt;
                self.vel_y[p] += acc[1][p] * dt;
                self.vel_z[p] += acc[2][p] * dt;
                self.pos_x[p] += self.vel_x[p] * dt;
                self.pos_y[p] += self.vel_y[p] * dt;
                self.pos_z[p] += self.vel_z[p] * dt;
            }
        } else {
            println!(
                "Integration method with {:} sub-steps not coded yet...",
                integration_substeps
            );
        }
    }
    #[allow(clippy::too_many_arguments)]
    pub fn simulate(
        &mut self,
        nb_steps: usize,
        dt: f64,
        integration_substeps: usize,
        min_dist: f64,
        theta_ratio: f64,
        record_all: bool,
        live_feed: bool,
    ) {
        let mut counter = 0;
        for i in 0..nb_steps {
            self.next_step(dt, integration_substeps, min_dist, theta_ratio);
            if i % 5 == 1 {
                println!("it {:}", i);
                if record_all {
                    screenshot(
                        &format!("serie1/dust_cloud_sim_test{}", counter),
                        self.render_xy_view(1920, 1080, [-5.33, 5.33], [-3., 3.]),
                    );
                }
                if live_feed {
                    screenshot(
                        "dust_cloud_sim_test",
                        self.render_xy_view(200, 200, [-3., 3.], [-3., 3.]),
                    );
                }
                counter += 1;
            }
        }
    }
    pub fn render_xy_view(
        &self,
        width: usize,
        height: usize,
        x_bounds: [f64; 2],
        y_bounds: [f64; 2],
    ) -> Vec<Vec<u8>> {
        let mut image = vec![vec![0_u8; height]; width];
        let iter = self
            .pos_x
            .iter()
            .zip(self.pos_y.iter())
            .zip(self.pos_z.iter())
            .zip(self.point_mass.iter())
            .map(|(((x, y), z), m)| (*x, *y, *z, *m));
        for (x, y, _z, m) in iter {
            let i = ((x - x_bounds[0]) / (x_bounds[1] - x_bounds[0]) * (width as f64)) as isize;
            let j = ((y - y_bounds[0]) / (y_bounds[1] - y_bounds[0]) * (height as f64)) as isize;
            if i >= 0 && i < width as isize && j >= 0 && j < height as isize {
                image[i as usize][j as usize] += (200. * m + 50.) as u8;
            }
        }
        image
    }
    pub fn get_vertices(&self) -> Vec<Vertex> {
        self.pos_x
            .iter()
            .zip(self.pos_y.iter())
            .zip(self.pos_z.iter())
            .map(|((x, y), z)| Vertex {
                position: [*x as f32, *y as f32, *z as f32],
                color: [1., 1., 1.],
            })
            .collect()
    }
    pub fn get_positions(&self) -> Vec<cgmath::Vector3<f32>> {
        self.pos_x
            .iter()
            .zip(self.pos_y.iter())
            .zip(self.pos_z.iter())
            .map(|((x, y), z)| cgmath::Vector3 {
                x: *x as f32,
                y: *y as f32,
                z: *z as f32,
            })
            .collect()
    }
}
