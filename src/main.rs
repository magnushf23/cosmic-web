/// Cosmic Web - Galaxy simulation
use cosmic_web::run;
//use pixels::{Pixels, SurfaceTexture};
//use rand::prelude::*;
//use winit::event::{Event, WindowEvent};
//use winit::event_loop::{ControlFlow, EventLoop};
//use winit::window::WindowBuilder;

fn main() {
    pollster::block_on(run());
}

#[cfg(test)]
mod unit_tests {
    use cosmic_web::{barneshut::BarnesHutTree, cloud::Cloud, screenshot};

    #[test]
    pub fn dust_cloud_sim() {
        let mut dust = Cloud::new();
        //let omz = (0.01 * (10000. - 1.) / (0.5_f64.powi(3))).sqrt();
        dust.fill_ball(
            [0., 0., 0.],
            1.,
            10,
            [0.1, 0.],
            [0., 0., 0., 0.],
            [0., 0., 0.],
        );
        //dust.fill_ball([-1.5, -0.25, 0.], 0.5, 1000, [0.1, 0.], [0.,0.9,0.1,0.1], [0.,0.,omz]);
        //dust.fill_ball([1.5, 0.25, 0.], 0.5, 1000, [0.1, 0.], [-0.,-0.9,-0.1,0.1], [0.,0.,omz]);
        //dust.fill_disk([-1.5, -0.25, -0.5], 0.5, 10000, [0.05, 0.], [0., 2.5, 0., 0.]);
        //dust.fill_disk([1.5, 0.25, 0.5], 0.5, 10000, [0.05, 0.], [-0., -2.5, 0., 0.]);
        //dust.fill_disk([0., 0., 0.], 2., 1000, [0.1, 0.], [0., 0., 0., 0.]);
        //println!("nb particles : {:}", dust.size);
        dust.simulate(100, 0.0005, 1, 0.0005, 0.5, false, false);
        //dust.simulate(100, 0.0005, 1, 0.0005, 0.5, false, true);
        //dust.simulate(10000, 0.0005, 1, 0.0005, 0.5, true, false);

        let qi_amir = 1000000;
        let qi_autres = vec![100, 145, 80];
        let amir_c_est_le_plus_beau = qi_amir > *qi_autres.iter().max().unwrap();
        assert!(amir_c_est_le_plus_beau);
        screenshot(
            "dust_cloud_sim_test",
            dust.render_xy_view(500, 500, [-3., 3.], [-3., 3.]),
        );
    }

    #[test]
    pub fn barneshut_64() {
        let mut tree = BarnesHutTree::with_size([0., 0., 0.], 1.);
        let masses = vec![1.; 64];
        let positions = (0..4)
            .flat_map(|x| {
                (0..4).flat_map(move |y| {
                    (0..4).map(move |z| {
                        [
                            0.25 * (x as f64 + 0.5),
                            0.25 * (y as f64 + 0.5),
                            0.25 * (z as f64 + 0.5),
                        ]
                    })
                })
            })
            .collect::<Vec<_>>();
        tree.construct_vec(&positions, &masses);
        tree.print();
        let force = tree.compute_body_force(&[0.5, 0.5, 0.5], &1., &0., &0.);
        println!("Force : {:}, {:}, {:}", force[0], force[1], force[2]);
        assert!(force[0].abs() < 1e-13 && force[1].abs() < 1e-13 && force[2].abs() < 1e-13)
    }

    #[test]
    fn test_image_plot() {
        match std::fs::remove_file("test.png") {
            Ok(()) => (),
            Err(e) => println!("Could not remove file because of : {:?}", e),
        };

        // Create a new ImgBuf with width: imgx and height: imgy
        let mut imgbuf = image::ImageBuffer::new(1000, 1000);
        // Iterate over the coordinates and pixels of the image
        for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
            let r = (0.3 * x as f32) as u8;
            let b = (0.3 * y as f32) as u8;
            *pixel = image::Rgb([r, 0, b]);
        }
        // Save the image as “fractal.png”, the format is deduced from the path
        imgbuf.save("test.png").expect("Problem on saving image");
    }
}
