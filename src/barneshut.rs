/// Octree structure for the Barnes-Hut algorithm
///
/// The tree is described recursively by it's root node and pointers
/// to it's eight `BarnesHutTree` children. Each node holds
/// information about it's children's center of gravity and total mass,
/// as well as the position and size of the portion of 3D space (cube)
/// represented by the tree.
///
#[derive(Debug, Default, Clone)]
pub struct BarnesHutTree {
    /// Total mass inside the tree.
    pub mass: f64,
    /// Center of mass of the tree.
    pub centre_mass: [f64; 3],
    /// Position of the cube's first corner.
    pub position: [f64; 3],
    /// The cell's edge length.
    pub cell_size: f64,
    /// The tree's eight children. If `children` is empty, the tree has either one particle or none (depending on the total mass).
    pub children: Vec<BarnesHutTree>,
}

impl BarnesHutTree {
    /// Default initialization, empty tree located in (0,0,0), with an edge length of 1.
    pub fn new() -> Self {
        BarnesHutTree {
            mass: 0.,
            centre_mass: [0., 0., 0.],
            position: [0., 0., 0.],
            cell_size: 1.,
            children: Vec::<BarnesHutTree>::new(),
        }
    }

    /// Initialization with custom position and edge size.
    pub fn with_size(corner_position: [f64; 3], size: f64) -> Self {
        BarnesHutTree {
            mass: 0.,
            centre_mass: [0., 0., 0.],
            position: corner_position,
            cell_size: size,
            children: Vec::<BarnesHutTree>::new(),
        }
    }

    /// Recursively inserts a new particle with a given mass and position in the octree.
    pub fn add_obj(&mut self, mass: &f64, position: &[f64; 3]) {
        if self.mass == 0. {
            // The node is empty, fill it with a single particle
            self.mass = *mass;
            self.centre_mass[0] = position[0];
            self.centre_mass[1] = position[1];
            self.centre_mass[2] = position[2];
        } else if !self.children.is_empty() {
            // Node already subdivided, pass the new particle to the correct child
            let w = self.cell_size / 2.;
            let x = self.position[0];
            let y = self.position[1];
            let z = self.position[2];
            // New particle's child's indices
            let new_particle_i = (position[0] - x >= w) as usize;
            let new_particle_j = (position[1] - y >= w) as usize;
            let new_particle_k = (position[2] - z >= w) as usize;
            // Convert to 1D child index
            let new_particle_index = new_particle_i + 2 * new_particle_j + 4 * new_particle_k;

            // Pass to correct child
            self.children[new_particle_index].add_obj(mass, position);
            // Update node's mass
            let total_mass = self.mass + mass;
            self.centre_mass[0] =
                (self.centre_mass[0] * self.mass + position[0] * mass) / total_mass;
            self.centre_mass[1] =
                (self.centre_mass[1] * self.mass + position[1] * mass) / total_mass;
            self.centre_mass[2] =
                (self.centre_mass[2] * self.mass + position[2] * mass) / total_mass;
            self.mass = total_mass;
        } else {
            // The node holds one particle already, divide it and redistribute mass
            let w = self.cell_size / 2.;
            let x = self.position[0];
            let y = self.position[1];
            let z = self.position[2];

            // Divide each edge in half, in 3D --> 8 childs
            self.children
                .push(BarnesHutTree::with_size(self.position, w));
            self.children
                .push(BarnesHutTree::with_size([x + w, y, z], w));
            self.children
                .push(BarnesHutTree::with_size([x, y + w, z], w));
            self.children
                .push(BarnesHutTree::with_size([x + w, y + w, z], w));
            self.children
                .push(BarnesHutTree::with_size([x, y, z + w], w));
            self.children
                .push(BarnesHutTree::with_size([x + w, y, z + w], w));
            self.children
                .push(BarnesHutTree::with_size([x, y + w, z + w], w));
            self.children
                .push(BarnesHutTree::with_size([x + w, y + w, z + w], w));

            // First particle's child's indices
            let old_particle_i = (self.centre_mass[0] - x >= w) as usize;
            let old_particle_j = (self.centre_mass[1] - y >= w) as usize;
            let old_particle_k = (self.centre_mass[2] - z >= w) as usize;
            // Convert to 1D child index
            let old_particle_index = old_particle_i + 2 * old_particle_j + 4 * old_particle_k;

            // Second particle's child's indices
            let new_particle_i = (position[0] - x >= w) as usize;
            let new_particle_j = (position[1] - y >= w) as usize;
            let new_particle_k = (position[2] - z >= w) as usize;
            // Convert to 1D child index
            let new_particle_index = new_particle_i + 2 * new_particle_j + 4 * new_particle_k;

            // Add both to corresponding childs
            self.children[old_particle_index].add_obj(&self.mass, &self.centre_mass);
            self.children[new_particle_index].add_obj(mass, position);

            // Update node's mass and center of mass
            let total_mass = self.mass + mass;
            self.centre_mass[0] =
                (self.centre_mass[0] * self.mass + position[0] * mass) / total_mass;
            self.centre_mass[1] =
                (self.centre_mass[1] * self.mass + position[1] * mass) / total_mass;
            self.centre_mass[2] =
                (self.centre_mass[2] * self.mass + position[2] * mass) / total_mass;
            self.mass = total_mass;
        }
    }

    /// Constructs a complete tree from a list of positions and masses
    pub fn construct_vec(&mut self, particles: &[[f64; 3]], masses: &[f64]) {
        for (particle, mass) in particles.iter().zip(masses.iter()) {
            self.add_obj(mass, particle)
        }
        //println!("Current box size : {:}", self.cell_size);
    }
    /// Constructs a complete tree from lists of X,Y,Z positions and masses
    pub fn construct(&mut self, pos_x: &[f64], pos_y: &[f64], pos_z: &[f64], masses: &[f64]) {
        let iter = pos_x
            .iter()
            .zip(pos_y.iter())
            .zip(pos_z.iter())
            .zip(masses.iter())
            .map(|(((x, y), z), m)| (x, y, z, m));
        for (&x, &y, &z, mass) in iter {
            self.add_obj(mass, &[x, y, z]);
        }
        //println!("Current box size : {:}", self.cell_size);
    }
    /// Computes the gravitional force exerted by the tree on a certain object.
    pub fn compute_body_force(
        &self,
        position: &[f64; 3],
        g_mass: &f64,
        min_distance: &f64,
        theta_ratio: &f64,
    ) -> [f64; 3] {
        if self.mass == 0. {
            // First case : node is empty --> no force
            [0., 0., 0.]
        } else if self.children.is_empty()
            && (position[0] != self.centre_mass[0]
                || position[1] != self.centre_mass[1]
                || position[2] == self.centre_mass[2])
        {
            // Second case : node holds one particle (different from the one we test)
            let invdist3 = (((self.centre_mass[0] - position[0]).powi(2)
                + (self.centre_mass[1] - position[1]).powi(2)
                + (self.centre_mass[2] - position[2]).powi(2))
            .sqrt()
                + min_distance)
                .recip()
                .powi(3);
            let fact = g_mass * self.mass * invdist3;
            [
                (self.centre_mass[0] - position[0]) * fact,
                (self.centre_mass[1] - position[1]) * fact,
                (self.centre_mass[2] - position[2]) * fact,
            ]
        } else {
            let dist = ((self.centre_mass[0] - position[0]).powi(2)
                + (self.centre_mass[1] - position[1]).powi(2)
                + (self.centre_mass[2] - position[2]).powi(2))
            .sqrt()
                + min_distance;
            if self.cell_size / dist < *theta_ratio {
                // Third case : s/d < theta --> force approximation using the center of mass of the current subtree
                let fact = g_mass * self.mass / dist.powi(3);
                [
                    (self.centre_mass[0] - position[0]) * fact,
                    (self.centre_mass[1] - position[1]) * fact,
                    (self.centre_mass[2] - position[2]) * fact,
                ]
            } else {
                // Fourth case : compute force on children and add the contributions together
                let mut force = [0.; 3];
                for child in self.children.iter() {
                    let res = child.compute_body_force(position, g_mass, min_distance, theta_ratio);
                    force[0] += res[0];
                    force[1] += res[1];
                    force[2] += res[2];
                }
                force
            }
        }
    }
    /// Prints the tree.
    pub fn print(&self) {
        print!("Barnes-Hut Tree :\nRoot");
        self.deep_print("", -1);
    }
    fn deep_print(&self, prefix: &str, index: i8) {
        if self.mass == 0. {
        } else if !self.children.is_empty() {
            println!("{}-› Child {:}", prefix, index + 1);
            for (id, child) in self.children.iter().enumerate() {
                child.deep_print(&format!("{:} |", prefix), id as i8);
            }
            println!("{} °", prefix);
        } else {
            println!(
                "{}-• Particle: mass = {:}, pos = [{:},{:},{:}]",
                prefix, self.mass, self.position[0], self.position[1], self.position[2]
            );
        }
    }
}
